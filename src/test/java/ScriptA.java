import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

/*
Скрипт А. Логин в Админ панель
        1. Открыть страницу Админ панели
        2. Ввести логин, пароль и нажать кнопку Логин.
        3. После входа в систему нажать на пиктограмме пользователя в верхнем
        правом углу и выбрать опцию «Выход.»
*/

public class ScriptA {
    public static void main(String[] args) {
        WebDriver driver = getDriver();
        driver.manage().window().maximize();

        //Открыть страницу Админ панели
        driver.get("http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/");
        //Ввести логин, пароль и нажать кнопку Логин.
        //Логин: webinar.test@gmail.com
        WebElement login_form = driver.findElement(By.id("email"));
        login_form.sendKeys("webinar.test@gmail.com");
        //Пароль: Xcg7299bnSmMuRLp9ITw
        WebElement password = driver.findElement(By.id("passwd"));
        password.sendKeys("Xcg7299bnSmMuRLp9ITw");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        WebElement submit = driver.findElement(By.name("submitLogin"));
        submit.click();
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //После входа в систему нажать на пиктограмме пользователя в верхнем правом углу и выбрать опцию «Выход.»

        WebElement profile_button = driver.findElement(By.id("employee_infos"));
        profile_button.click();

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        WebElement logout = driver.findElement(By.id("header_logout"));
        logout.click();

        driver.close();
    }

    public static WebDriver getDriver(){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\vvelichko\\IdeaProjects\\adsf\\resources\\chromedriver.exe");
        //System.out.println(System.getProperty("user.dir") + "\\resources\\chromedriver.exe");
        //System.setProperty("wedriver.chrome.driver", System.getProperty("user.dir") + "\\resources\\chromedriver.exe");
        return new ChromeDriver();
    }
}
